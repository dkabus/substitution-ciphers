# Simple Substitution Ciphers in Python

by Desmond Kabus

In this repository, I present my implementations of some substitution ciphers in the `python` file `ciphers.py`:

- A1Z26 cipher: `(4)(5)(19)(13)(15)(14)(4)`
- Atbash cipher: `wvhnlmw`
- Caesar cipher: `ghvprqg`
- Vigenere cipher: `scltcas` (with key `python`)

and a symbolic cipher, which substitutes each letter by a symbol.

These ciphers **are not at all** cryptographically secure.
In fact, in the `analyse.py`, I wrote a simple dictionary attack, to crack these encryptions.

## Applications

I used these scripts to decrypt encrypted messages, which appeared in the (great) TV shows:

- [Futurama](https://en.wikipedia.org/wiki/Futurama)
- and [Gravity Falls](https://en.wikipedia.org/wiki/Gravity_Falls)

Both of these shows also contain symbolic ciphers.
I created a font for each of these ciphers.
Also, I decrypted the symbol-ciphers only using a dictionary attack.
That is only with the knowledge, that the encrypted language is English.

Therefore, I independently verified the translation tables found in:

- [AL1 on *The Infosphere* (Futurama Wiki)](https://theinfosphere.org/Alien_languages#AL1)
- [Ciphers on the *Gravity Falls Wiki*](https://gravityfalls.fandom.com/wiki/List_of_cryptograms)

## Installation

You need `python3` and several Python-Packages to run my scripts.

- Look up, how to install Python [here](https://www.python.org/).
- The packages, you need to install are listed on the top of the `.py` files.
- The simplest way, to install a python package is:
    `pip3 install <package-name>`
- However, depending on your computer's operating system, other installation methods might be preferable.

## Usage

The scripts are relatively well documented, just look at the help-option:
    `python3 ciphers.py -h`

## License

The included license applies only to the software, and not to the encrypted content `.enc` files.
The content remains property of their respective owners and is used only for educational purposes, which makes it fair use.
