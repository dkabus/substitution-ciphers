export DIR := $(shell pwd)
export CONVERT := $(DIR)/ciphers.py
export RM := rm -f
subdirs := a1z26 atbash caesar symbol_gravity_falls_author symbol_gravity_falls_bill symbol_futurama

targets := gravity_falls_journal3_library.md gravity_falls_s2e18_title.md

all: $(targets)
	@ for subdir in $(subdirs) ; do \
		$(MAKE) -C $$subdir ; \
	done

gravity_falls_journal3_library.md: gravity_falls_journal3_library.enc $(CONVERT)
	$(CONVERT) -d vigenere -k pines -i $< -o $@

gravity_falls_s2e18_title.md: gravity_falls_s2e18_title.enc $(CONVERT)
	$(CONVERT) -d vigenere -k bluebook -i $< -o $@

clean:
	$(RM) $(targets)
	@ for subdir in $(subdirs) ; do \
		$(MAKE) -C $$subdir clean ; \
	done
