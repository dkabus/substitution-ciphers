#!/usr/bin/env python3
from ciphers import *
from glob import glob
from collections import Counter

def run(cmd, input):
    """
        run `cmd` in terminal with `input` piped in as stdin
    """
    from subprocess import Popen, PIPE
    process = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    output, _ = process.communicate(input=input.encode())
    return output.decode()

def ratio_misspelled(text):
    """
        run the `hunspell` spellchecker and
        return fraction of characters which are in misspelled words
    """
    return int(run("hunspell -l | wc -c", text))/len(text)

def spelling_fitness(key, cipher_text):
    """
        calculate the fitness by using a spellchecker
        on the decrypted `cipher_text` using `key`
    """
    text = key.translate(cipher_text, back=True, shortcuts=False)
    return 1. - ratio_misspelled(text)

class CachedFunction:
    """
        Function wrapper to cache the function values
        of a (deterministic) function f(x) with one hashable argument
    """
    def __init__(self, func):
        self.func = func
        self.cache = dict()
        self.num_queries = 0
        self.num_evals = 0

    def __call__(self, x):
        h = hash(x)
        f = self.cache.get(h)
        self.num_queries += 1
        if f is None:
            self.num_evals += 1
            f = self.func(x)
            self.cache[h] = f
        return f

    def fraction_evals(self):
        return self.num_evals/self.num_queries

@CachedFunction
def fitness(key):
    return spelling_fitness(key, " ".join(ct))

def most_common_letters(words):
    return "".join(dict(Counter("".join(words)).most_common()).keys())

def read_data(key_filename, cipher_text_glob, english_glob="english_texts/*.md"):
    """
        read files containing english texts, cipher texts, and the key
    """
    en = "\n".join([read(f).lower() for f in glob(english_glob)])
    ct = "\n".join([read(f) for f in glob(cipher_text_glob)])
    key_orig = SymbolPermutation(key_filename)

    # clean english data and split into words
    seps = set(c for c in en if not c.isalpha())
    en = en.translate(str.maketrans(dict(zip(seps, (" " for _ in seps)))))
    en = "".join(c for c in en if c.isalpha() or c == " ")
    en = [word for word in en.split(" ") if len(word) > 0]

    # clean cipher text data: convert to bca words
    ct = ct.replace("\n", " ")
    ct = key_orig.translate(ct, apply_key=False, shortcuts=True, back=True)
    ct = [word for word in ct.split(" ") if len(word) > 0]
    return key_orig, ct, en

def histogram_match(template_key, cipher_text, english_text):
    """
        create a key by matching the most common letters
    """
    abc = most_common_letters(english_text)
    bca = most_common_letters(cipher_text)
    bca = bca.replace("?", "") + "".join(a for a in abc if a not in bca)
    return template_key.copy().init(dictionary=dict(zip(abc, bca)))

def otheandi_match(template_key, cipher_text):
    """
        create a key by guessing the letters
        - *o* (from common two letter words)
        - *a* and *i* (the only one letter words in English)
        - *and* (common three letter word without *i* and *o*
            starting with *a*)
        - *the* (common three letter word
            without *a*, *d*, *i*, *n*, and *o*)
    """
    common_words_by_length = [
        dict(Counter(w for w in ct if len(w) == i).most_common(4))
        for i in range(0, 4)
    ]

    random_choice = lambda x: (len(x) > 0) and np.random.choice(list(x)) or None

    letters = dict()

    # letter "o"
    letters["o"] = random_choice(
        most_common_letters(common_words_by_length[2])
    )

    # word "and"
    and_c = random_choice([
        w for w in common_words_by_length[3]
        if w[0] in common_words_by_length[1]
        and ("o" not in letters or letters["o"] not in w)
        and len(set(w)) == 3
    ])
    if and_c is not None:
        letters["a"] = and_c[0]
        letters["n"] = and_c[1]
        letters["d"] = and_c[2]

    # letter "i"
    if len(common_words_by_length[1]) >= 2:
        letters["i"] = random_choice([
            w for w in common_words_by_length[1]
            if w != letters.get("a")
        ])

    # word "the"
    the_c = random_choice([
        w for w in common_words_by_length[3]
        if len(set(w)) == 3
        and ("o" not in letters or letters["o"] not in w)
        and ("a" not in letters or letters["a"] not in w)
        and ("n" not in letters or letters["n"] not in w)
        and ("d" not in letters or letters["d"] not in w)
        and ("i" not in letters or letters["i"] not in w)
    ])
    if the_c is not None:
        letters["t"] = the_c[0]
        letters["h"] = the_c[1]
        letters["e"] = the_c[2]

    # collect results
    abc, bca = "", ""
    for a in "otheandi":
        b = letters.get(a)
        if b is not None:
            abc, bca = abc + a, bca + b

    print(f"the \"otheandi\" function matched these {len(abc)} letters: {abc}")

    # merge missing letters from template_key
    abc = abc + "".join(c for c in template_key.abc() if c not in abc)
    bca = bca + "".join(c for c in template_key.bca() if c not in bca)
    return template_key.copy().init(dictionary=dict(zip(abc, bca)))

def simple_genetic_algorithm(fitness, x):
    """
        run a simple (1+1)-GA
    """
    print("running (1+1)-GA, cancel with [CTRL]+[c]")
    opti = 0
    try:
        while True:
            x_ = x.copy().mutate()
            if fitness(x_) > fitness(x):
                x = x_
                print(f"{opti:04d}: fitness = {fitness(x)*100:2.1f}%, x = {x}")
            opti += 1
    except KeyboardInterrupt:
        pass
    return x

folders = ["symbol_gravity_falls_bill", "symbol_gravity_falls_author", "symbol_futurama"]
folder = folders[2]
key_orig, ct, en = read_data(folder+"/key.csv", folder+"/*.enc")
key1 = histogram_match(key_orig, ct, en)
key2 = otheandi_match(key1, ct)
key = simple_genetic_algorithm(fitness, key2.copy())

print("\n# Final Result")
print(key)
print(table_to_string(key.table()))
write("result.key.csv", table_to_string(key.table(format="{}"), header=False))

print("\n# Evaluations")
print("fitness_evals =", fitness.num_evals)
print("fraction_evals =", fitness.fraction_evals())

print("\n# Comparison")
print("letter match:",
    f"{fitness(key1)*100:2.1f}%", key1)
print("\"otheandi\":  ",
    f"{fitness(key2)*100:2.1f}%", key2)
print("result:      ",
    f"{fitness(key)*100:2.1f}%", key)
print("key on disk: ",
    f"{fitness(key_orig)*100:2.1f}%", key_orig)
