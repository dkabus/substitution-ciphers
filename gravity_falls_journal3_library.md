hey, dipper and mabel!

blendin here. i'm currently living in the year 1883, and i hid
this letter in hopes that it would one day reach you. (i got
the idea from the movie "return backwards to the past again 3,"
which is required viewing for time academy freshmen.) you've probably
got a lot of questions, and after the events of weirdmageddon, man, i
don't blame you!

so here's what happened: after globnar, i was getting a lot of flak
for losing a gladiator fight to two children. even though i got my
job back (thanks for that, by the way!), my fellow officers kept calling
me hurtful nicknames. time baby himself called me no-friendin
blandin! do you know what it's like to live like that?

just as i was thinking i would give anything for time baby's
respect, this weird triangle guy showed up in my dreams and said
that he would make sure time baby never bothered me again. all
i had to do was shake his hand! i've never been great at making
decisions under pressure, and, well, you know the rest! when i
awoke, that stupid triangle had used my body to travel through time
and destroy the entire universe-and time baby, too!

luckily, time baby isn't dead... exactly. it will take one
thousand years for his molecules to reconstitute, and when they do,
boy, is he gonna be cranky! the time agents also survived-they only
send their holo-projections out on dangerous missions. but
lolph and dundgren were furious about what i did!

of course, this means that there is a dimension-wide manhunt of
agents trying to find me and bring me to justice. but it was all a
mistake!

the last thing i want is to go back to jail, so i've been hiding
out in the past. it was fun for a while-seeing the sights,
chatting with historical figures, visiting the '50s to try to learn
the twist. (i accidentally twisted my ankle and couldn't walk for a
week.)

i wanted to check out the old west, too, but when i traveled here, i
accidentally appeared right in front of a train and my time tape was
shattered into fifty pieces. (also, i think the train might have
gone off the rails. hopefully that isn't in the history books!)

anyway, i just wanted to say not to worry about me! i've managed
to blend in to the populace, and i got a job as a pocket watch
repairman. i've also caught one of those cool gold rush-era
diseases that're so popular in these times, and i am loving
the nostalgia! thanks for all your help, ane if any time agents come
looking for me, tell them you don't know nothing!

blendin blenjamin blandin, 1883
