#!/usr/bin/env python3
import optparse
import numpy as np
import csv
import re
import copy
import sys

class Char:
    def __init__(self, char=None, num=None):
        if char is not None:
            self.num = self.to_num(char)
        elif num is not None:
            self.num = num
        else:
            raise ValueError("Cannot initialize Char")

    def to_num(self, char=None):
        if char is None:
            return self.num
        else:
            return ord(char) - ord("a")

    def to_chr(self, num=None):
        num = num or self.num
        return chr(num + ord("a"))

    def __str__(self):
        return self.to_chr()

    def __len__(self):
        return self.num

    def __call__(self):
        return self.num

class Translator:
    def abc(self, length=None):
        if length is None:
            length = len(self)
        return "".join(str(Char(num=i)) for i in range(length))

    def bca(self):
        raise NotImplementedError("implemented in subclasses")

    def dict(self, flipped=False):
        if flipped: return dict(zip(self.bca(), self.abc()))
        else:       return dict(zip(self.abc(), self.bca()))

    def translate(self, string, lower=True, back=False):
        if lower: string = string.lower()
        d = self.dict(flipped=back)
        return self._apply_translation_dict(string, d)

    def _apply_translation_dict(self, s, d, use_re=None):
        if use_re is None:
            use_re = getattr(self, "use_re", False)

        if use_re:
            regex = re.compile("|".join(map(re.escape, d.keys())))
            return regex.sub(lambda mo: d[mo.group(0)], s)
        else:
            return s.translate(str.maketrans(d))

    def copy(self):
        return copy.deepcopy(self)

    def table(self, *args, **kwargs):
        return dict(m=self.abc(), c=self.bca())

class CharKey(Translator):
    def __init__(self, offset=1):
        # must use regex, since replacements are longer than one character
        self.use_re = True
        self.offset = offset

    def __len__(self):
        return 26

    def bca(self):
        return ["("+str(self.offset+Char(c).to_num())+")" for c in self.abc()]

    def __str__(self):
        return self.__class__.__name__

class Permutation(Translator):
    """
        This class implements permutations of length $N$.
        Permutations are bijective functions
        from {0, 1, ..., N-1} to {0, 1, ..., N-1}.
    """
    def __init__(self, *args, **kwargs):
        "passes arguments to `self.init`"
        self.init(*args, **kwargs)

    def init(self, length=None, dictionary=None, bca=None, perm=None):
        """
            Try to initialize using different inputs, in this order:
            1. initialize using `dictionary`
            2. initialize using permuted alphabe `bca`
            3. initialize using `perm` an array of the indices
            4. initialize trivially for given length
        """
        if dictionary is not None:
            bca = "".join(dictionary[c]
                for c in self.abc(length=len(dictionary)))

        if bca is not None:
            assert(bca.isalpha())
            bca = bca.lower()
            self.perm = np.array([Char(char).to_num() for char in bca], dtype=int)
        elif perm is not None:
            self.perm = np.array(perm, dtype=int)
        elif length is not None:
            self.perm = np.arange(length)
        else:
            raise ValueError("Cannot initialize Permutation")
        self.validate()
        return self

    def __len__(self):
        return len(self.perm)

    def __hash__(self):
        return hash(self.bca())

    def __str__(self):
        return f"{self.__class__.__name__}(length={len(self)}, bca=\"{self.bca()}\")"

    def __call__(self, i):
        return self.perm[i]

    def permute(self, x):
        assert(len(x) == len(self))
        return [x[self(i)] for i in range(len(x))]

    def bca(self):
        return "".join(self.permute(self.abc()))

    def shuffle(self):
        np.random.shuffle(self.perm)
        return self

    def roll(self, n):
        self.perm = np.roll(self.perm, n)
        return self

    def reverse(self):
        self.perm = np.flip(self.perm)
        return self

    def swap(self, *indices):
        indices = set(indices)
        values = np.roll([self.perm[i] for i in indices], 1)
        for i, v in zip(indices, values):
            self.perm[i] = v
        return self

    def mutate(self):
        r = np.random.randint
        self.swap(*(r(len(self)) for i in range(r(2,5))))
        self.validate()
        return self

    def validate(self):
        for i in range(len(self)):
            assert(i in self.perm)
        return self

class SymbolPermutation(Permutation):
    def __init__(self, filename, image_format="![](img/{}.png)"):
        with open(filename, "r") as f:
            table = [
                [cell for cell in row if len(cell) > 0]
                for row in csv.reader(f, delimiter=" ") if len(row) > 0
            ]

        # for consistency
        table.sort(key=lambda row: row[1])

        # add extra column (bca)
        table = [(row[0], a, *row[1:])
            for a, row in zip(self.abc(length=26), table)]

        # create dictionary for permutation
        abc = iter(row[0] for row in table)
        bca = iter(row[1] for row in table)
        perm = dict(zip(abc, bca))

        # create dictionary for shortcuts
        bca = iter(row[1] for row in table)
        sct = iter(row[2] for row in table)
        self.shortcuts = dict(zip(bca, sct))

        # create dictionary for image filenames
        sct = iter(row[2] for row in table)
        img = iter(row[3] for row in table)
        self.images = dict(zip(sct, img))

        self.image_format = image_format

        # initialize the permutation
        self.init(dictionary=perm)

    def translate(self, string, back=False, lower=True, apply_key=True, shortcuts=True):
        tra_dicts = []
        if apply_key:
            tra_dicts.append(self.dict())

        if shortcuts:
            tra_dicts.append(self.shortcuts)

        if back:
            tra_dicts = [dict(zip(d.values(), d.keys()))
                for d in reversed(tra_dicts)]
            lower = False

        if lower:
            string = string.lower()

        for d in tra_dicts:
            string = self._apply_translation_dict(string, d)

        return string

    def image_filenames(self, format=None):
        if format is None: format = self.image_format
        return dict(zip(self.images.keys(), (format.format(v) for v in self.images.values())))

    def preview(self, string, **kwargs):
        # must use regex, since replacements are longer than one character
        string = self._apply_translation_dict(string, self.image_filenames(**kwargs), use_re=False)
        return string

    def table(self, *args, **kwargs):
        abc = self.abc()
        bca = self.bca()
        sct = self.translate(bca, apply_key=False, shortcuts=True)
        images = self.image_filenames(*args, **kwargs)
        preview = [images[s] for s in sct]
        return dict(m=abc, c=sct, img=preview)

class MultiKey:
    """
        Combine multiple keys into one.
        Encryption and decryption uses one key after another.
    """
    def __init__(self, keys):
        self.keys = keys

    def __len__(self):
        return len(self.keys)

    def __str__(self):
        s = self.__class__.__name__ + "(keys=[\n"
        s += ",\n".join(str(k) for k in self.keys)
        s += "])"
        return s

    def get_key(self, i):
        return self.keys[i%len(self.keys)]

    def __call__(self, x):
        return [self.get_key(i)(x[i]) for i in range(len(x))]

    def translate(self, string, *args, only_count_letters=True, **kwargs):
        new = ""
        i = 0
        for char in string:
            if char.isalpha() or not only_count_letters:
                new += self.get_key(i).translate(char, *args, **kwargs)
                i += 1
            else:
                new += char
        return new

class MultiPermutation(MultiKey):
    def __init__(self, keyword=None, length=None, letters=26):
        if keyword is not None:
            self.keys = [
                Permutation(letters).roll(-Char(c).to_num())
            for c in keyword.lower()]
        elif length is not None:
            self.keys = [Permutation(letters) for i in range(length)]
        else:
            raise ValueError("Cannot initialize MultiPermutation")

    def shuffle(self):
        for key in self.keys:
            key.shuffle()

    def reverse(self):
        for key in self.keys:
            key.reverse()

class Cipher:
    def __init__(self, key):
        self.key = key

    def encrypt(self, message):
        return self.key.translate(message)

    def decrypt(self, message):
        return self.key.translate(message, back=True)

    def __str__(self):
        return self.__class__.__name__ + " with key: " + str(self.key)

    def table(self, *args, **kwargs):
        return self.key.table(*args, **kwargs)

class A1Z26(Cipher):
    def __init__(self):
        self.key = CharKey()

class Atbash(Cipher):
    def __init__(self):
        self.generate()

    def generate(self, letters=26):
        self.key = Permutation(length=letters)
        self.key.reverse()

class Caesar(Cipher):
    def __init__(self, shift=-3):
        if type(shift) == type("c"):
            if shift.isalpha():
                shift = -Char(shift).to_num()
            else:
                shift = int(shift)
        self.generate(shift=shift)

    def generate(self, shift=None, letters=26):
        self.key = Permutation(length=letters)
        if shift is None:
            shift = np.random.randint(letters)
        self.key.roll(shift)

class Vigenere(Cipher):
    def __init__(self, *args, **kwargs):
        self.generate(*args, **kwargs)
    def generate(self, *args, **kwargs):
        self.key = MultiPermutation(*args, **kwargs)

class Symbol(Cipher):
    def __init__(self, filename=None):
        self.generate(filename=filename)

    def generate(self, filename=None):
        if filename is None:
            self.key = Permutation(length=26)
        else:
            self.key = SymbolPermutation(filename=filename)

    def preview(self, message):
        return self.key.preview(message)

def test(m="Mr. Upsidedownington"):
    print("Running test")
    print("m =", m)

    ciphers = [A1Z26(), Caesar(), Atbash(), Vigenere("hello")]
    for Pi in ciphers:
        print("\n" + Pi.__class__.__name__ + ":")

        print("k =", Pi.key)

        c = Pi.encrypt(m)
        print("c =", c)

        m_ = Pi.decrypt(c)
        print("m =", m_)

        assert(m.lower() == m_)

def read(filename=None):
    with filename is None and sys.stdin or open(filename, "r") as f:
        return "".join(f).replace("\r", "")

def write(filename=None, text=""):
    with filename is None and sys.stdout or open(filename, "w") as f:
        f.write(text)

def get_cipher(name, key=None, ciphers=[]):
    try:
        cipher = ciphers[name.lower()]
    except KeyError:
        raise KeyError("Cipher name not recognized: " + name)
    if key is None:
        cipher = cipher()
    else:
        cipher = cipher(key)
    return cipher

def table_to_string(table, header=True):
    widths = [max(len(el) for el in column) for column in table.values()]
    rowstr = lambda row: " ".join(f"{v:{w}}" for v, w in zip(row, widths))
    lines = []
    if header:
        lines.append(rowstr(table.keys()))
        lines.append(rowstr(["".join("-" for a in range(w)) for w in widths]))
    for row in zip(*table.values()):
        lines.append(rowstr(row))
    return "\n".join(lines)

if __name__ == "__main__":
    ciphers = [A1Z26, Vigenere, Caesar, Atbash, Symbol]
    help_text_ciphers = [cipher.__name__ for cipher in ciphers]
    help_text_ciphers = ", ".join(help_text_ciphers)
    help_text_ciphers = ", one of: " + help_text_ciphers

    cipher_names = [cipher.__name__.lower() for cipher in ciphers]
    ciphers = dict(zip(cipher_names, ciphers))

    parser = optparse.OptionParser()
    parser.add_option(
        "-T", "--test",
        action="store_true", default=False,
        help="test all ciphers"
    )
    parser.add_option(
        "-i", "--input",
        help="read input from FILE",
        metavar="FILE",
        action="store", type="string", default=None,
    )
    parser.add_option(
        "-o", "--output",
        help="write results to FILE",
        metavar="FILE",
        action="store", type="string", default=None,
    )
    parser.add_option(
        "-k", "--key",
        help="which KEY to use for encryption/decryption",
        metavar="KEY",
        action="store", type="string", default=None,
    )
    parser.add_option(
        "-e", "--encrypt",
        help="encrypt using CIPHER" + help_text_ciphers,
        metavar="CIPHER",
        action="store", type="string", default=None,
    )
    parser.add_option(
        "-d", "--decrypt",
        help="decrypt using CIPHER" + help_text_ciphers,
        metavar="CIPHER",
        action="store", type="string", default=None,
    )
    parser.add_option(
        "-t", "--table",
        help="create a markdown table for CIPHER" + help_text_ciphers,
        metavar="CIPHER",
        action="store", type="string", default=None,
    )
    parser.add_option(
        "-p", "--symbol-preview",
        help="create a markdown file replacing the utf8 representation of the symbol cipher by png-files using CIPHER",
        metavar="CIPHER",
        action="store", type="string", default=None,
    )
    parser.add_option(
        "-q", "--quiet",
        help="hide progress messages",
        action="store_true", default=False,
    )

    (options, args) = parser.parse_args()
    verbose = not options.quiet

    if options.test:
        test()
        exit()

    if options.key is not None:
        key = options.key
        if verbose: print("using key:", key)
    else:
        key = None

    if options.table is not None:
        cipher = get_cipher(options.table, key, ciphers)
        if verbose: print("creating markdown table for:", cipher)
        text = table_to_string(cipher.table(format="![](img/{}.png){{width=24px}}"))
    else:
        if verbose: print("reading input file:", options.input or "stdin")
        text = read(options.input)

    if options.symbol_preview is not None:
        cipher = get_cipher(options.symbol_preview, key, ciphers)
        if verbose: print("creating markdown symbol preview for cipher:", cipher)
        text = cipher.preview(text)

    if options.encrypt is not None:
        cipher = get_cipher(options.encrypt, key, ciphers)
        if verbose: print("encrypting using cipher:", cipher)
        text = cipher.encrypt(text)

    if options.decrypt is not None:
        cipher = get_cipher(options.decrypt, key, ciphers)
        if verbose: print("decrypting using cipher:", cipher)
        text = cipher.decrypt(text)

    if verbose: print("writing result to output file:", options.output or "stdout")
    write(options.output, text)
